/**
 * Pelicula.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'localDiskDb',
  attributes: {
	id: {
            type: 'integer',
            autoIncrement: true,
            primaryKey: true
        },
        show_title: {
            type: 'string'
        },
        release_year: {
            type: 'string'
        },
	category: {
            type: 'string'
        },
	show_cast: {
	    type: 'string'
	},
	summary: {
	    type: 'string'
	},
	poster: {
            type: 'string'
        }
  }
};

